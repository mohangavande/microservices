package com.mohan.microservices.limitsservice.bean;

public class LimiConfiguration {
	
	private int maximum;
	private int minimum;
	
	protected LimiConfiguration() {
		
	}
	
	public LimiConfiguration(int maximum, int minimum) {
		super();
		this.maximum = maximum;
		this.minimum = minimum;
	}

	public int getMaximum() {
		return maximum;
	}

	public int getMinimum() {
		return minimum;
	}
	
	

}
