package com.mohan.microservices.limitsservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mohan.microservices.limitsservice.bean.LimiConfiguration;

@RestController
public class LimitConfigurationController {

	@GetMapping(path="/limits")
	public LimiConfiguration retriveLimitConfigurations() {
		
		return new LimiConfiguration(1000, 1);
	}
}
